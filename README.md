# Brazilian ID Validator

This is a frontend appication to validate, save, edit and delete CPF/CNPJ's of the following Server Project: [brazilian-id-validator-server](https://bitbucket.org/lucasghizoni/brazilian-id-validator-server).

The app is built using the new Web Components specifications, with [Polymer](https://github.com/Polymer/polymer).


## Viewing Your Application

First, you need to have the Server up, more instructions at: [brazilian-id-validator-server](https://bitbucket.org/lucasghizoni/brazilian-id-validator-server). Now with the server up, supposing you have Docker installed, just type:

```
$ sudo make run
```

When instalation finishes, it will print the docker ip, with the address you need to access on your browser, as it follows: 

Available on:
  http://172.17.0.2:8080

Obs: If you have any troubles with Docker, just install latest version of node, and type: 

```
$ npm install
$ npm start
```


## CustomElements overview

To help understanding the component structure of the application, here we have a little overview of the main CustomElements created:

```html
<br-id-validator-app>
	<custom-form>
		<!-- More custom-elements and native elements here for making our form  -->
	</custom-form>
	<custom-grid id="individuals">
		<!-- More custom-elements and native elements here for making the individuals grid  -->
	</custom-grid>
	<custom-grid id="companies">
		<!-- More custom-elements and native elements here for making the companies grid  -->
	</custom-grid>
</br-id-validator-app>
```

## Tests

### Component Unit Tests

To run the component tests, you need to have the front server up, as it was explained before. With the front server up, just access:

http://127.0.0.1:8080/test/unit/test-suite.html

To test in different browsers, just access this URL in the browser you wish.


### Selenium Tests

To run the selenium tests, you will need the [brazilian-id-validator-server](https://bitbucket.org/lucasghizoni/brazilian-id-validator-server) up. You will need the front server up as well.
After that, type the following commands:

```
$ npm install
$ npm test
```

To run with chrome headless:

```
$ npm test headless
```

#### Report

After running, you can see a report HTML at: test/selenium/report/mochawesome.html


Obs: To run these tests, you must need node, npm and Google Chrome installed on your machine.
It seems that something has changed in how webdriver interacts with find webcomponents on page, so it was not possible to write more tests.

## Known issues and improvements:

- Make a Docker container to run selenium tests. In this container you will need the backend server, frontend server and Chrome headless.
- Pagination on <custom-grid>. If you have many data on grids without a pagination, you will have serious performance problems.
- Responsive layout.


- BUG: If you type very fast on <custom-inputs>, the autocomplete with ".", "-" and "/" is not working very well.