const By = require('selenium-webdriver').By;

module.exports = class CustomFormElement {
    
    constructor(driver, locator){
        this.driver = driver;
        this.locator = locator;
    }

    getElement(){
        return this.driver.findElement(this.locator);
    }

    typeId(value){
        const input = this.getElement().findElement(By.xpath('.//custom-input//input'));
        return input.sendKeys(value);
    }

}