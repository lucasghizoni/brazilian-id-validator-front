const webdriver = require('selenium-webdriver'),
      until = webdriver.until,
      By = webdriver.By;

const chai = require('chai'),
      expect = require('chai').expect,
      request = require('request');

const CustomFormElement = require('./lib/page/CustomFormElement');

// loading chromedriver
require('chromedriver');

const builder = new webdriver.Builder();
const chromeCapabilities = webdriver.Capabilities.chrome();

let args = ["--start-maximized"];

if(process.argv.indexOf('headless') !== -1){
    args = args.concat(["--headless", "--disable-gpu"]);
}

chromeCapabilities.set('chromeOptions', { "args": args});
builder.withCapabilities(chromeCapabilities);

let driver = builder.build();

describe('App tests', function () {
    this.timeout(10000);
    
    beforeEach(function(done){
        driver.get('http://localhost:8080').then(()=> {
            done();
        });
    });

    it.only('Verify if main custom element br-id-validator-app is created', function (done) {
        driver.findElements(By.xpath("//br-id-validator-app")).then( mainCustomElements => {
            expect(mainCustomElements.length).to.equal(1);
            done();
        });
    });

    it('Save a CPF', function (done) {
        //TODO
        // const customFormElm = new CustomFormElement(driver, By.xpath("//custom-form"));
        // customFormElm.typeId("068.754.469-60");
    });

    after(function(){
        driver.quit();
    });
});

