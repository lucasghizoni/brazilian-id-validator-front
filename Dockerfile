FROM node:latest
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY package.json /usr/src/app/
COPY bower.json /usr/src/app/
RUN npm install
RUN ./node_modules/bower/bin/bower install --allow-root
RUN npm install -g http-server --allow-root
COPY . /usr/src/app
EXPOSE 8080
CMD [ "http-server", "-c-1 -a localhost" ]
